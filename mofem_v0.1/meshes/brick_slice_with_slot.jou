reset

###### FILE DESCRIPTION ##############
# 
# JOURNAL FILE FOR TEST 8S2 of reference
# R P Birkett and C J Jones GCDMC P135 AEAT-2736 Issue 1
# Hinkley Point B and Hunterston Geometry 
# Internal loading scenario 
# Created 20/6/2014 by Owen Booler AMEC 
# Last Modified 20/6/2014 by Owen Booler AMEC
#
###### GEOMETRY##################
#
# creating the 45 degree sector Hunterston B fuel brick Geometry 
create curve location 0 0 0 location 131.625 0 0
create curve location 131.625 0 0 location 192.18 0 0
create curve location 192.18 0 0 location 192.18 10.125 0
create curve location 192.18 10.125 0 location 229.9 10.125 0
create curve location 229.9 10.125 0 location 229.9 45.72995 0
curve 1  copy rotate 45 about z 
create curve location at vertex 12   direction curve 6  length 60.555
create curve location at vertex 14   direction 1 -1 0 length 16.475
create curve location at vertex 16   direction 1 1 0 length 37.72
create curve location at vertex 18   direction 1 -1 0 length 29.255
create curve location at vertex 20   location at vertex 10  
create curve arc center vertex 11  12  2  
delete Curve 1
delete Curve 6
create surface curve 7 12 3 2 4 5 11 10 9 8  
#
# Projecting 45 degree sector to make a 100mm thick octant
# change distance to vary the thickness units in mm
sweep surface 1  direction z  distance 100 
#
# Copy octant to create full 360 degree slice
Volume 1  copy reflect y 
Volume 1 2 copy rotate 90 about z 
Volume 1 2 copy rotate 180 about z 
Volume 1 2 copy rotate 270 about z 
unite volume all 
#
# Put fillets into keyway corners radius = 1.6
modify curve 17 99 27 53 38 188 203 234 219 128 143 174 159 68 114 83  blend radius 1.6
# 
# Create Slot 
#
create vertex 0 -250 0 
create vertex 0 0 0 
create curve vertex 239 240  
curve 307  copy rotate -5 about z 
curve 307  copy rotate 5 about z 
delete Curve 307
create curve vertex 241 243  
create surface curve 308 309 310  
sweep surface 113  perpendicular distance 100
subtract volume 9  from volume 1  
#
# Create loading pin holes
create vertex 0 0 0 
create vertex 0 -260 0 
create curve vertex 259 258  
create vertex 0 -153.75 0 
create vertex 0 -207.5 0 
create curve arc center vertex 260 8 10 radius 5 full
create curve arc center vertex 261 8 10 radius 5 full
Curve 331 332 333  copy rotate -13 about z 
Curve 331 332 333  copy rotate 13 about z 
delete Curve 331 332 333 334 337
delete Vertex 260
delete Vertex 261
create surface curve 338  
create surface curve 339  
create surface curve 336  
create surface curve 335  
sweep surface 125 126 127 128 direction z distance 100
subtract volume 10 11 12 13 from volume 1
#
# Create loading assembly 
#
create brick x 50 y 243 z 11.43
rotate Curve 362  angle 5  about Z include_merged
move Curve 366  midpoint location curve 318  include_merged 
Volume 14  copy move x 0 y 0 z -100 
move Surface 149  location surface 142  except x y include_merged 
create cylinder height 100 radius 5
create vertex center curve 357
create vertex center curve 354
move Surface 157  location vertex 302  include_merged   
create cylinder height 100 radius 5
move Surface 160  location vertex 303  include_merged 
delete vertex 302 303
unite volume 14 15 16 17 
#
brick x 40 y 191.43 z 20
rotate Volume 18  angle -5  about Z include_merged 
align Volume 18  surface 164  with surface 141 
move Volume 18  midpoint location curve 322  except z include_merged 
create curve vertex 256 254  
move Curve 404 midpoint x -5.7 include_merged 
move Curve 396  midpoint location curve 404 except y z include_merged
move curve 396 midpoint x 5.7 include_merged
Volume 18  copy move z -100 
move Surface 169  location surface 142  except x y include_merged 
delete Curve 404
create Cylinder height 100 radius 5 
create vertex center curve 348  
create vertex center curve 351  
move Surface 177  location vertex 330  except x y include_merged 
move Surface 177  location vertex 330  include_merged 
create Cylinder height 100 radius 5 
move Surface 180  location vertex 331  include_merged 
unite volume 21 18 19 20  
delete Vertex 330 331
#
# Create initial crack 
#
create vertex on curve 262  midpoint  
#create curve location at vertex 336   direction surface 32  length -6 #-3  surface 101
create curve location vertex 336 direction 1 -3 0 length 6
create vertex on curve 260  midpoint   
#create curve location at vertex 338   direction surface 32  length -6 #-3 surface 101 
create curve location vertex 338 direction 1 -3 0 length 6
create curve vertex 338 336     
create curve vertex 337 339     
create surface curve 425 423 426 424     
create Cylinder height 100 radius 6 #3  
move Surface 186  location vertex 336  include_merged    
chop volume 1  with volume 23     
webcut volume 24  with plane surface 183     
delete surface 183   
#   
#   
# Rotate all volumes by 5degrees to allow loading frame to be in the x plane   
rotate Volume all angle -5  about Z include_merged    
#   
# Loading surfaces    
create vertex on curve 360  fraction 0.105820106 from vertex 284     
create vertex on curve 364  fraction 0.105820106 from vertex 289   
create curve vertex 362 363    
imprint tolerant surface 148   with curve 461  merge    
#   
create vertex on curve 372  fraction 0.105820106 from vertex 292   
create vertex on curve 376  fraction 0.105820106 from vertex 297   
create curve vertex 366 367      
imprint tolerant surface 154   with curve 467  merge   
delete curve 461 467   
#   
create vertex on curve 464  fraction 0.170940174 from vertex 285     
create vertex on curve 362  fraction 0.152851264 from vertex 286     
create curve vertex 370 371   
imprint tolerant surface 143   with curve 473  merge   
#   
create vertex on curve 469  fraction 0.170940174 from vertex 296     
create vertex on curve 378  fraction 0.152851264 from vertex 299   
create curve vertex 374 375   
imprint tolerant surface 150   with curve 479  merge   
delete curve 473 479   
#   
#partition create surface 143  curve 469    
#delete Curve 469   
#create vertex on curve 466  fraction 0.170940174 from vertex 293     
#create vertex on curve 374  fraction 0.152851264 from vertex 294     
#create curve vertex 374 375   
#partition create surface 161  curve 473   
#delete curve 473      
#   
#   
# Merge volumes    
imprint all    
merge all    
#   
#   
##### MATERIAL PROPERTIES ######################   
#   
# Graphite Brick Slice   
#   
block 1 volume 26 24 25     
block 1 attribute count 10   
block 1 attribute index 1 1.09   
block 1 attribute index 2 0.2   
block 1 name 'MAT_ELASTIC_GRAPHITE'   
#   
# Steel loading Pins   
#   
block 2 volume 14 21     
block 2 name 'MAT_ELASTIC_STEEL'   
block 2 attribute count 10   
block 2 attribute index 1 21  
block 2 attribute index 2 0.3   
#   
##### BOUNDARY CONDITIONS #####################   
#   
# Fixed Boundary condition    
create Displacement  on surface 170  dof all fix 0   
#   
# Loading    
create force  on surface 208  force value -4.3e-1 direction  x    
create force  on surface 210  force value -4.3e-1 direction  x    
create force  on surface 211  force value 4.33e-1 direction  x    
create force  on surface 214  force value 4.33e-1 direction  x    
#   
##### FRACTURE CONSTRAINTS #####################   
#   
Create Block Set to plot load vs dsplacement graphts   
block 3 vertex 356 357  
block 3 name 'LoadPath'  
#  
# SIDESET 100 - ALL EDGES NOT CRACK   
Sideset 100 curve all    
sideset 100 curve 445 446 447 448 429 431 451 459 452 457 remove   
#   
#SIDESET 102 - ALL SURFACES NOT CRACK   
sideset 102 surface all   
sideset 102 surface 138 137 139 140 224 222 216 220 200 204 163 197 remove   
#   
#SIDESET 200 - CRACK SURFACE   
sideset 200 surface 197   
#   
# SIDESET 201 - CRACK FRONT   
sideset 201 curve 445   
#   
# NODESET 101 - ALL VERTICIES NOT CRACK   
nodeset 101 vertex all   
nodeset 101 vertex 356 354 355 357 remove   
#   
#   
##### MESH DEFINITIONS ############################   
#   
volume all scheme Tetmesh    
#   
#GLOBAL REFINEMENT   
volume all size auto factor 4  
#volume 24 26 size auto factor 10  
volume 24 26 size 16  
#volume 14 21 size auto factor 5   
mesh volume all    
#   
###### SAVE MODEL ########################   
#   
# Adapt this to the file and directory you wish to save    
#   
#save as "/projects/symphonyb/MOFEM_Dev/model dev/AGR_Brick/HNB_8S2/Test_8S2.cub" overwrite   
#   
#   
###### END OF FILE #########################   














