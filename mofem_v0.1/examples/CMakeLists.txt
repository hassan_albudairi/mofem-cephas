# Copyright (C) 2013, Lukasz Kaczmarczyk (likask AT wp.pl)
# The MoFEM package is copyrighted by Lukasz Kaczmarczyk. 
# It can be freely used for educational and research purposes 
# by other institutions. If you use this softwre pleas cite my work. 
#
# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

#include subdirectory
include_directories("${PROJECT_SOURCE_DIR}/examples")

#add executable library form subdirectory
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/elasticity")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/thermal")

add_subdirectory("${PROJECT_SOURCE_DIR}/examples/interface")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/dynamics")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/nonlinear_elasticity")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/arc_length_nonlinear_elasticity")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/arc_length_nonlinear_interface")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/mesh_smoothing")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/material_forces")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/potential_flow")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/RVE_trans")
add_subdirectory("${PROJECT_SOURCE_DIR}/examples/ultraweak")



add_subdirectory("${PROJECT_SOURCE_DIR}/examples/meshkit_basic_example")


