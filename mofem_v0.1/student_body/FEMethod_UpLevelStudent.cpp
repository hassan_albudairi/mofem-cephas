/* Copyright (C) 2013, Lukasz Kaczmarczyk (likask AT wp.pl)
 * --------------------------------------------------------------
 * FIXME: DESCRIPTION
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include "FEMethod_UpLevelStudent.hpp"
#include "FEM.h"

namespace MoFEM {

FEMethod_UpLevelStudent::FEMethod_UpLevelStudent(Interface& _moab,int _verbose): FEMethod_LowLevelStudent(_moab,_verbose) {
  double def_V = 0;
  rval = moab.tag_get_handle("Volume",1,MB_TYPE_DOUBLE,th_volume,MB_TAG_CREAT|MB_TAG_SPARSE,&def_V); CHKERR_THROW(rval);
}
FEMethod_UpLevelStudent::~FEMethod_UpLevelStudent() {}
PetscErrorCode FEMethod_UpLevelStudent::OpStudentStart_TET(vector<double>& _gNTET_) {
  PetscFunctionBegin;
    fe_ent_ptr = fePtr->fe_ptr;
    ierr = InitDataStructures(); CHKERRQ(ierr);
    ierr = GlobIndices(); CHKERRQ(ierr);
    ierr = LocalIndices(); CHKERRQ(ierr);
    ierr = DataOp(); CHKERRQ(ierr);
    ierr = ShapeFunctions_TET(_gNTET_); CHKERRQ(ierr);
  try {
    ierr = Data_at_GaussPoints(); CHKERRQ(ierr);
    ierr = DiffData_at_GaussPoints(); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "thorw in GetColNMatrix_at_GaussPoint(): " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  try {
    ierr = GetRowNMatrix_at_GaussPoint(); CHKERRQ(ierr);
    ierr = GetColNMatrix_at_GaussPoint(); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "thorw in GetColNMatrix_at_GaussPoint(): " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  try {
    ierr = GetRowDiffNMatrix_at_GaussPoint(); CHKERRQ(ierr);
    ierr = GetColDiffNMatrix_at_GaussPoint(); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "thorw in GetRowDiffNMatrix_at_GaussPoint(): " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  EntityHandle fe_handle = fePtr->get_ent();

  V = Shape_intVolumeMBTET(diffNTET,&*coords.data().begin()); 
  if( V <= 0 ) {
    SETERRQ(PETSC_COMM_SELF,1,"negative volume");
    throw FEMethod_UpLevelStudent_ExceptionNegatvieTetVolume();
  }
  rval = moab.tag_set_data(th_volume,&fe_handle,1,&V); CHKERR_PETSC(rval);
  const int g_dim = get_dim_gNTET();
  coords_at_Gauss_nodes.resize(g_dim);
  for(int gg = 0;gg<g_dim;gg++) {
    coords_at_Gauss_nodes[gg].resize(3);
    for(int dd = 0;dd<3;dd++) {
      (coords_at_Gauss_nodes[gg])[dd] = cblas_ddot(4,&coords[dd],3,&get_gNTET()[gg*4],1);
    }
  }

  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::OpStudentStart_PRISM(vector<double>& _gNTRI_) {
  PetscFunctionBegin;
  fe_ent_ptr = fePtr->fe_ptr;
  ierr = InitDataStructures(); CHKERRQ(ierr);
  ierr = GlobIndices(); CHKERRQ(ierr);
  ierr = DataOp(); CHKERRQ(ierr);

  ierr = ShapeFunctions_PRISM(_gNTRI_); CHKERRQ(ierr);
  ierr = GetRowNMatrix_at_GaussPoint(); CHKERRQ(ierr);
  ierr = GetColNMatrix_at_GaussPoint(); CHKERRQ(ierr);

  ierr = ShapeDiffMBTRI(diffNTRI); CHKERRQ(ierr);

  int num_nodes;
  const EntityHandle *conn_prism;
  rval = moab.get_connectivity(fe_ent_ptr->get_ent(),conn_prism,num_nodes,true); CHKERR_PETSC(rval);
  assert(num_nodes==6);
  for(int nn = 0;nn<3;nn++) {
    conn_face3[nn] = conn_prism[nn];
    conn_face4[nn] = conn_prism[nn+3];
    //cerr << conn_face3[nn] << " ::: " << conn_face4[nn] << endl;
  }

  //face3
  rval = moab.get_coords(conn_face3,3,coords_face3); CHKERR_PETSC(rval);
  ierr = ShapeFaceNormalMBTRI(diffNTRI,coords_face3,normal3); CHKERRQ(ierr);
  area3 = cblas_dnrm2(3,normal3,1)*0.5;
  //face4
  rval = moab.get_coords(conn_face4,3,coords_face4); CHKERR_PETSC(rval);
  ierr = ShapeFaceNormalMBTRI(diffNTRI,coords_face4,normal4); CHKERRQ(ierr);
  area4 = cblas_dnrm2(3,normal4,1)*0.5;

  /*copy(&normal3[0],&normal3[3],ostream_iterator<double>(cerr," "));
  cerr << " -->  ";
  copy(&normal4[0],&normal4[3],ostream_iterator<double>(cerr," "));
  cerr << " : " << cblas_ddot(3,normal3,1,normal4,1)/(area3*area4) <<  endl;*/

  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::OpStudentEnd() {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetRowGlobalIndices(const string &field_name,vector<DofIdx> &RowGlobDofs) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ1(PETSC_COMM_SELF,1,"no such field <%s>",field_name.c_str());
  Indices_Type::iterator miit = row_nodesGlobIndices.find(fiit->get_MoFEMField_ptr());
  if(miit == row_nodesGlobIndices.end()) {
    ostringstream sss;
    sss << "no such field in FE!" << endl;
    sss << "(top tip) check  of feName " << feName << " if is have field_name " << field_name << endl;
    sss << "see if this element can be not on this part" << endl;
    SETERRQ(PETSC_COMM_SELF,1,sss.str().c_str());
  }
  RowGlobDofs = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetRowGlobalIndices(const string &field_name,EntityType type,vector<DofIdx> &RowGlobDofs,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	    RowGlobDofs.resize(0);
	    PetscFunctionReturn(0);
	  }
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else {
	  RowGlobDofs.resize(0);
	  PetscFunctionReturn(0); 
	}
      }
      switch(type) {
	case MBEDGE: {
	  Indices_EntType::iterator miit = row_edgesGlobIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_edgesGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  RowGlobDofs = miit->second;
	} break;
	case MBTRI: {
	  Indices_EntType::iterator miit = row_facesGlobIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_facesGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  RowGlobDofs = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == rowPtr->get<Composite_Name_And_Type_mi_tag>().end()) {
	RowGlobDofs.resize(0);
	PetscFunctionReturn(0);
      }
      Indices_EntType::iterator miit = row_elemGlobIndices.find(eiit->get_MoFEMEntity_ptr());
      if(miit == row_elemGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      RowGlobDofs = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetRowLocalIndices(const string &field_name,vector<DofIdx> &RowLocalDofs) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  Indices_Type::iterator miit = row_nodesLocalIndices.find(fiit->get_MoFEMField_ptr());
  if(miit == row_nodesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  RowLocalDofs = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetRowLocalIndices(const string &field_name,EntityType type,vector<DofIdx> &RowLocalDofs,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	    RowLocalDofs.resize(0);
	    PetscFunctionReturn(0);
	  }
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else {
	  RowLocalDofs.resize(0);
	  PetscFunctionReturn(0); 
	}
      }
      switch(type) {
	case MBEDGE: {
	  Indices_EntType::iterator miit = row_edgesLocalIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_edgesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  RowLocalDofs = miit->second;
	} break;
	case MBTRI: {
	  Indices_EntType::iterator miit = row_facesLocalIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_facesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  RowLocalDofs = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == rowPtr->get<Composite_Name_And_Type_mi_tag>().end()) {
	RowLocalDofs.resize(0);
	PetscFunctionReturn(0);
      }
      Indices_EntType::iterator miit = row_elemLocalIndices.find(eiit->get_MoFEMEntity_ptr());
      if(miit == row_elemLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      RowLocalDofs = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetColGlobalIndices(const string &field_name,vector<DofIdx> &ColGlobDofs) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  Indices_Type::iterator miit = col_nodesGlobIndices.find(fiit->get_MoFEMField_ptr());
  if(miit == col_nodesGlobIndices.end()) {
    ostringstream sss;
    sss << "no such field in FE!" << endl;
    sss << "(top tip) check column of feName " << feName << " if is have field_name " << field_name << endl;
    sss << "see if this element can be not on this part" << endl;
    SETERRQ(PETSC_COMM_SELF,1,sss.str().c_str());
  }
  ColGlobDofs = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetColGlobalIndices(const string &field_name,EntityType type,vector<DofIdx> &ColGlobDofs,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	    ColGlobDofs.resize(0);
	    PetscFunctionReturn(0);
	  }
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else {
	  ColGlobDofs.resize(0);
	  PetscFunctionReturn(0); 
	}
      }
      switch(type) {
	case MBEDGE: {
	  Indices_EntType::iterator miit = col_edgesGlobIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_edgesGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  ColGlobDofs = miit->second;
	} break;
	case MBTRI: {
	  Indices_EntType::iterator miit = col_facesGlobIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_facesGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  ColGlobDofs = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == colPtr->get<Composite_Name_And_Type_mi_tag>().end()) {
	ColGlobDofs.resize(0);
	PetscFunctionReturn(0);
      }
      Indices_EntType::iterator miit = col_elemGlobIndices.find(eiit->get_MoFEMEntity_ptr());
      if(miit == col_elemGlobIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      ColGlobDofs = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetColLocalIndices(const string &field_name,vector<DofIdx> &ColLocalDofs) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  Indices_Type::iterator miit = col_nodesLocalIndices.find(fiit->get_MoFEMField_ptr());
  if(miit == col_nodesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE!\n(see this element can be not on this part)");
  ColLocalDofs = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetColLocalIndices(const string &field_name,EntityType type,vector<DofIdx> &ColLocalDofs,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	  if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	    ColLocalDofs.resize(0);
	    PetscFunctionReturn(0);
	  }
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else {
	  ColLocalDofs.resize(0);
	  PetscFunctionReturn(0); 
	}
      }
      switch(type) {
	case MBEDGE: {
	  Indices_EntType::iterator miit = col_edgesLocalIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_edgesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  ColLocalDofs = miit->second;
	} break;
	case MBTRI: {
	  Indices_EntType::iterator miit = col_facesLocalIndices.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_facesLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  ColLocalDofs = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == colPtr->get<Composite_Name_And_Type_mi_tag>().end()) {
	ColLocalDofs.resize(0);
	PetscFunctionReturn(0);
      }
      Indices_EntType::iterator miit = col_elemLocalIndices.find(eiit->get_MoFEMEntity_ptr());
      if(miit == col_elemLocalIndices.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      ColLocalDofs = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetDataVector(const string &field_name,ublas::vector<FieldData> &Data) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  Data_Type::iterator miit = data_nodes.find(fiit->get_MoFEMField_ptr());
  if(miit == data_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  Data = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetDataVector(const string &field_name,EntityType type,ublas::vector<FieldData> &Data,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FEDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = dataPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == dataPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = dataPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"no such ent");
	} else SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      }
      switch(type) {
	case MBEDGE: {
	  Data_EntType::iterator miit = data_edges.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == data_edges.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  Data = miit->second;
	} break;
	case MBTRI: {
	  Data_EntType::iterator miit = data_faces.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == data_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  Data = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FEDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = dataPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == dataPtr->get<Composite_Name_And_Type_mi_tag>().end()) PetscFunctionReturn(0);//SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      Data_EntType::iterator miit = data_elem.find(eiit->get_MoFEMEntity_ptr());
      if(miit == data_elem.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      Data = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussDataVector(const string &field_name,vector< ublas::vector<FieldData> > &Data) {
  PetscFunctionBegin;
  H1L2_Data_at_Gauss_pt::iterator miit = h1l2_data_at_gauss_pt.find(field_name);
  if(miit == h1l2_data_at_gauss_pt.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  Data = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussDiffDataVector(const string &field_name,vector< ublas::matrix<FieldData> > &Data) {
  PetscFunctionBegin;
  H1_DiffData_at_Gauss_pt::iterator miit = h1_diff_data_at_gauss_pt.find(field_name);
  if(miit == h1_diff_data_at_gauss_pt.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  Data = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussRowNMatrix(const string &field_name,vector< ublas::matrix<FieldData> > &NMatrix) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  N_Matrix_Type::iterator miit = row_N_Matrix_nodes.find(fiit->get_MoFEMField_ptr());
  if(miit == row_N_Matrix_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  NMatrix = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussRowNMatrix(const string &field_name,EntityType type,vector< ublas::matrix<FieldData> > &NMatrix,int side_number) {
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      }
      switch(type) {
	case MBEDGE: {
	  N_Matrix_EntType::iterator miit = row_N_Matrix_edges.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_N_Matrix_edges.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  NMatrix = miit->second;
	} break;
	case MBTRI: {
	  N_Matrix_EntType::iterator miit = row_N_Matrix_faces.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_N_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  NMatrix = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == rowPtr->get<Composite_Name_And_Type_mi_tag>().end()) PetscFunctionReturn(0);//SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      N_Matrix_EntType::iterator miit = row_N_Matrix_elem.find(eiit->get_MoFEMEntity_ptr());
      if(miit == row_N_Matrix_elem.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      NMatrix = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussColNMatrix(const string &field_name,vector< ublas::matrix<FieldData> > &NMatrix) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  N_Matrix_Type::iterator miit = col_N_Matrix_nodes.find(fiit->get_MoFEMField_ptr());
  if(miit == col_N_Matrix_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  NMatrix = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussColNMatrix(const string &field_name,EntityType type,vector< ublas::matrix<FieldData> > &NMatrix,int side_number) {
  PetscFunctionBegin;
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      }
      switch(type) {
	case MBEDGE: {
	  N_Matrix_EntType::iterator miit = col_N_Matrix_edges.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_N_Matrix_edges.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  NMatrix = miit->second;
	} break;
	case MBTRI: {
	  N_Matrix_EntType::iterator miit = col_N_Matrix_faces.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_N_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  NMatrix = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == colPtr->get<Composite_Name_And_Type_mi_tag>().end()) PetscFunctionReturn(0);//SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      N_Matrix_EntType::iterator miit = col_N_Matrix_elem.find(eiit->get_MoFEMEntity_ptr());
      if(miit == col_N_Matrix_elem.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      NMatrix = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussRowDiffNMatrix(const string &field_name,vector< ublas::matrix<FieldData> > &diffNMatrix) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  N_Matrix_Type::iterator miit = row_diffN_Matrix_nodes.find(fiit->get_MoFEMField_ptr());
  if(miit == row_diffN_Matrix_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  diffNMatrix = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussRowDiffNMatrix(const string &field_name,EntityType type,vector< ublas::matrix<FieldData> > &diffNMatrix,int side_number) {
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent");
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      }
      switch(type) {
	case MBEDGE: {
	  N_Matrix_EntType::iterator miit = row_diffN_Matrix_edges.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_diffN_Matrix_edges.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  diffNMatrix = miit->second;
	} break;
	case MBTRI: {
	  N_Matrix_EntType::iterator miit = row_diffN_Matrix_faces.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == row_diffN_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  diffNMatrix = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = rowPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == rowPtr->get<Composite_Name_And_Type_mi_tag>().end()) PetscFunctionReturn(0);//SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      N_Matrix_EntType::iterator miit = row_diffN_Matrix_elem.find(eiit->get_MoFEMEntity_ptr());
      if(miit == row_diffN_Matrix_elem.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      diffNMatrix = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussColDiffNMatrix(const string &field_name,vector< ublas::matrix<FieldData> > &diffNMatrix) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  N_Matrix_Type::iterator miit = col_diffN_Matrix_nodes.find(fiit->get_MoFEMField_ptr());
  if(miit == col_N_Matrix_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
  diffNMatrix = miit->second;
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussColDiffNMatrix(const string &field_name,EntityType type,vector< ublas::matrix<FieldData> > &diffNMatrix,int side_number) {
  switch(type) {
    case MBEDGE:
    case MBTRI: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number));
      if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	if((fePtr->get_ent_type()==MBPRISM)&&(type==MBEDGE)&&(side_number>=6)) {
	  eiit = colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,type,side_number-6));
	  if(eiit == colPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent");
	  if(eiit->side_number_ptr->brother_side_number!=side_number) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
	} else SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      }
      switch(type) {
	case MBEDGE: {
	  N_Matrix_EntType::iterator miit = col_diffN_Matrix_edges.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_N_Matrix_edges.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  diffNMatrix = miit->second;
	} break;
	case MBTRI: {
	  N_Matrix_EntType::iterator miit = col_diffN_Matrix_faces.find(eiit->get_MoFEMEntity_ptr());
	  if(miit == col_N_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
	  diffNMatrix = miit->second;
	} break;
	default:
	SETERRQ(PETSC_COMM_SELF,1,"no implemented");
      }
    } break;
    case MBTET:
    case MBPRISM: {
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_And_Type_mi_tag>::type::iterator eiit;
      eiit = colPtr->get<Composite_Name_And_Type_mi_tag>().find(boost::make_tuple(field_name,type));
      if(eiit == colPtr->get<Composite_Name_And_Type_mi_tag>().end()) PetscFunctionReturn(0);//SETERRQ(PETSC_COMM_SELF,1,"no such ent");
      N_Matrix_EntType::iterator miit = col_diffN_Matrix_elem.find(eiit->get_MoFEMEntity_ptr());
      if(miit == col_N_Matrix_elem.end()) SETERRQ(PETSC_COMM_SELF,1,"no such ent in FE");
      diffNMatrix = miit->second;
    } break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::MakeBMatrix3D(
  const string &field_name,vector<ublas::matrix<FieldData> > &diffNMatrix,vector<ublas::matrix<FieldData> > &BMatrix) {
  PetscFunctionBegin;
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ(PETSC_COMM_SELF,1,"no such field");
  if(fiit->get_space() != H1) SETERRQ(PETSC_COMM_SELF,1,"it has to be H1 space");
  if(fiit->get_max_rank() != 3) SETERRQ(PETSC_COMM_SELF,1,"it has to be rank 3");
  const int g_dim = get_dim_gNTET();
  BMatrix.resize(g_dim);
  for(int gg = 0;gg<g_dim;gg++) {
    ublas::matrix<FieldData> &diffMat = diffNMatrix[gg];
    ublas::matrix<FieldData> &BMat = BMatrix[gg];
    unsigned int m = diffMat.size1();
    unsigned int n = diffMat.size2();
    if(m != 9)  SETERRQ(PETSC_COMM_SELF,1,"wrong matrix size");
    if((BMat.size1()!=6)||(BMat.size2()!=n)) BMat.resize(6,n);
    // page 30 CHAPTER 6. DISPLACEMENT METHODS, FEAP Version 7.3 Theory Manual Robert L. Taylor
    // dX/dx (0) dX/dy (1) dX/dz (2); dY/dx (3) dY/dy (4) dY/dz (5); dZ/dx (6) dZ/dy (7) dZ/dz (8)
    cblas_dcopy(BMat.size2(),&diffMat.data()[0*diffMat.size2()],1,&BMat.data()[0*BMat.size2()],1);
    cblas_dcopy(BMat.size2(),&diffMat.data()[4*diffMat.size2()],1,&BMat.data()[1*BMat.size2()],1);
    cblas_dcopy(BMat.size2(),&diffMat.data()[8*diffMat.size2()],1,&BMat.data()[2*BMat.size2()],1);
    //
    cblas_dcopy(BMat.size2(),&diffMat.data()[1*diffMat.size2()],1,&BMat.data()[3*BMat.size2()],1);
    cblas_daxpy(BMat.size2(),1.,&diffMat.data()[3*diffMat.size2()],1,&BMat.data()[3*BMat.size2()],1);
    //
    cblas_dcopy(BMat.size2(),&diffMat.data()[5*diffMat.size2()],1,&BMat.data()[4*BMat.size2()],1);
    cblas_daxpy(BMat.size2(),1.,&diffMat.data()[7*diffMat.size2()],1,&BMat.data()[4*BMat.size2()],1);
    //
    cblas_dcopy(BMat.size2(),&diffMat.data()[2*diffMat.size2()],1,&BMat.data()[5*BMat.size2()],1);
    cblas_daxpy(BMat.size2(),1.,&diffMat.data()[6*diffMat.size2()],1,&BMat.data()[5*BMat.size2()],1);
  }
  PetscFunctionReturn(0);
}
PetscErrorCode FEMethod_UpLevelStudent::GetGaussRowFaceNMatrix(
  EntityHandle ent,const string &field_name,
  vector< ublas::matrix<FieldData> > &NMatrix,
  EntityType type,EntityHandle edge_handle) {
  PetscFunctionBegin;
  N_Matrix_Type N_Matrix_nodes;
  N_Matrix_EntType N_Matrix_edges;
  N_Matrix_EntType N_Matrix_faces;
  ierr = GetNMatrix_at_FaceGaussPoint(ent,field_name,
    row_nodesGlobIndices,row_edgesGlobIndices,row_facesGlobIndices,
    N_Matrix_nodes,N_Matrix_edges,N_Matrix_faces,
    type,edge_handle); CHKERRQ(ierr);
  MoFEMField_multiIndex::index<FieldName_mi_tag>::type::iterator fiit = fieldsPtr->get<FieldName_mi_tag>().find(field_name);
  if(fiit==fieldsPtr->get<FieldName_mi_tag>().end()) SETERRQ1(PETSC_COMM_SELF,1,"no < %s > field",field_name.c_str());
  switch (type) {
    case MBVERTEX: {
      N_Matrix_Type::iterator miit = N_Matrix_nodes.find(fiit->get_MoFEMField_ptr());
      if(miit == col_N_Matrix_nodes.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
      NMatrix = miit->second;
      }
      break;
    case MBTRI: {
      int side_number;
      try {
	side_number = fe_ent_ptr->get_side_number_ptr(moab,ent)->side_number;
      } catch (const char* msg) {
	  SETERRQ(PETSC_COMM_SELF,1,msg);
      }
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator fiiit;
      fiiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,MBTRI,side_number));
      if(fiiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	SETERRQ1(PETSC_COMM_SELF,1,"no such ent (side_number = %u)",side_number);
      }
      N_Matrix_EntType::iterator miit = N_Matrix_faces.find(fiiit->get_MoFEMEntity_ptr());
      if(miit == N_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
      NMatrix = miit->second;
      }
      break;
    case MBEDGE: {
      int side_number;
      try {
	side_number = fe_ent_ptr->get_side_number_ptr(moab,edge_handle)->side_number;
      } catch (const char* msg) {
	  SETERRQ(PETSC_COMM_SELF,1,msg);
      }
      FENumeredDofMoFEMEntity_multiIndex::index<Composite_Name_Type_And_Side_Number_mi_tag>::type::iterator eiiit;
      eiiit = rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().find(boost::make_tuple(field_name,MBEDGE,side_number));
      if(eiiit == rowPtr->get<Composite_Name_Type_And_Side_Number_mi_tag>().end()) {
	SETERRQ1(PETSC_COMM_SELF,1,"no such ent (side_number = %u)",side_number);
      }
      N_Matrix_EntType::iterator miit = N_Matrix_edges.find(eiiit->get_MoFEMEntity_ptr());
      if(miit == N_Matrix_faces.end()) SETERRQ(PETSC_COMM_SELF,1,"no such field in FE");
      NMatrix = miit->second;
      }
      break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"no implemented");
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FEMethod_UpLevelStudent::GetHierarchicalGeometryApproximation(
  vector< ublas::matrix<FieldData> > &invH,vector< FieldData > &detH) {
  PetscFunctionBegin;
  invH.resize(0);
  detH.resize(0);
  /*{
    //debug
    ublas::vector<FieldData> DataNodes;
    ierr = GetDataVector("MESH_NODE_POSITIONS",DataNodes); CHKERRQ(ierr);
    cout << "DataNodes " << DataNodes << endl;
  }*/
  H1_DiffData_at_Gauss_pt::iterator miit = h1_diff_data_at_gauss_pt.find("MESH_NODE_POSITIONS");
  if(miit == h1_diff_data_at_gauss_pt.end()) PetscFunctionReturn(0);
  invH = miit->second;
  detH.resize( invH.size() );
  ublas::matrix<FieldData> _H_(3,3);
  for(unsigned int gg = 0;gg<invH.size();gg++) {
    ublas::noalias(_H_) = invH[gg];
    detH[gg] = Shape_detJac(&*_H_.data().begin());
    ierr = Shape_invJac(&*invH[gg].data().begin()); CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FEMethod_UpLevelStudent::GetHierarchicalGeometryApproximation_ApplyToDiffShapeFunction(
  int rank,vector< ublas::matrix<FieldData> > &invH,vector< ublas::matrix<FieldData> > &diffNMatrix) {
  PetscFunctionBegin;
  if(invH.size() == 0) PetscFunctionReturn(0);
  if(invH.size() != diffNMatrix.size()) SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
  unsigned int gg = 0;
  ublas::matrix<FieldData> _diffNMatrix__;
  for(;gg<diffNMatrix.size();gg++) {
    ublas::matrix<FieldData> &_invH_ = invH[gg];
    ublas::matrix<FieldData> &_diffNMatrix_ = diffNMatrix[gg];
    _diffNMatrix__.resize(_diffNMatrix_.size1(),_diffNMatrix_.size2());
    for(int rr = 0;rr<rank;rr++) {
      cblas_dgemm(
	CblasRowMajor,CblasTrans,CblasNoTrans,
	3,_diffNMatrix_.size2(),3,1.,
	&*_invH_.data().begin(),3,
	&_diffNMatrix_.data()[rank*_diffNMatrix_.size2()*rr],_diffNMatrix_.size2(),
	0.,
	&_diffNMatrix__.data()[rank*_diffNMatrix__.size2()*rr],_diffNMatrix__.size2()); 
    }
    ublas::noalias(_diffNMatrix_) = _diffNMatrix__;
  }
  PetscFunctionReturn(0);
}

}

