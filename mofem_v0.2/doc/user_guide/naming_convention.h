/*! \page coding_practice Coding practice

\section citizen Be a good MoFEM citizen

- MoFEM is a team work. Don't be focussed only on your project, try improve
  MOFEM. Your modifications can help others, others modification will help
  you.
- MoFEM is not own by anyone, you are become owner proportionally to your
  contribution. 
- If you find bugs,or you think that documentation can be improved, you are
  strongly encourage to contribute. 
- If you have idea how to improve MoFEM library, f.e. some functions
  names are not clear or new functionality is can be added, 
  propose that on our discussion group CMatGU <cmatgu@googlegroups.com>.

\section mofam_and_user_modules MoFEM and User Modules

All new finite elements or modification or finite elements are implemented in
users modules (UM). Only "reliable" and "proven" to work finite elements are
moved to MoFEM finite element library. Implementation to MoFEM library (not UM)
need to be always discussed, agreed and planed on CMatGU
<cmatgu@googlegroups.com>.  Work on UM is less controlled and contained to user
directory. It is planed that it will be single version of MoFEM library but
many UM forks. Some UM can be in developed independently from MoFEM library repository.

 In default UM are build as a part of MoFEM
library, i.e. source files from MoFEM source directory are used in compilation
process. UM can be created as "stand alone" source, when out of source build is
created as follows
\code
cmake -DSTAND_ALLONE_USERS_MODULES ../user_modules
\endcode 

\section user_module Adding user module

In each module you can have two type directory data, 
  -# Simple
  -# Extended

User module is added to \em ModulesLists.cmake file using cmake command:
\code
add_subdirectory(my_new_module)
\endcode
Simple directory structure consist no subdirectories. f.e. elasticity. Extended (recommended)
data structure consist subdirectories, f.e. homogenisation, and follows pattern

\code
-> /atom_tests
-> /src <- hpp files
-> /src/impl <- cpp files form library
-> /meshes
-> /data
-> /doc
\endcode

Not all elements of module source tree are compulsory, however each user module
and new MoFEM functionality should have associated \em atom \em test verifying
implementation and each module should have README file or module documentation
in doc using Doxygen.

\section Source Code Style and Best Practices

MoFEM code should follow MoAB code style and best pratices listed here
<http://www.mcs.anl.gov/~fathom/moab-docs/html/styleguide.html>.

- Style:

  - Make indentations. Indent code to better convey the logical structure of
    your code. Without indenting, code becomes difficult to follow. \code
if (...) { 
if (...) { 
...
} else {
...
}} else if {
... 
} else { 
... 
}
\endcode
Can you follow this ? This is much better \code
if (...) { 
  if (...) { 
    ...
  } else {
    ...
  }
} else if {
  ... 
} else { 
  ... 
}
\endcode

  - Enable syntax highlighting in your text editor.

  - Break large, complex sections of code into smaller, comprehensible modules
    (subroutine/functions/methods). A good rule is that modules do not exceed
    the size of the text editor window.

  - Indentation should have TWO SPACES. You have to set up your favorite editor to
    make TWO SPACES for TAB.

  - Use empty lines to provide organisational clues to source code, blocks
    (\em paragraphs -like structure) help the reader in comprehending the logical
    segmenting.

- Names:

  - A name should tell what rather than how, avoid names that expose underlying implementation.

  - Class names should be in the CamelBack style, e.g. EdgeMesh or VertexMesher.

  - Class member variables should be camelBack, e.g. EdgeMesh::schemeType; each
    member variable, e.g. int memberVariable, should have set/get functions 
    void member_variable(int newval) and int member_variable(), respectively

  - Enumeration values should be all capitalized, with underscores avoided if
    possible (the enumeration name indicates the general purpose of the
    enumeration, so e.g. we use EQUAL, not EQUAL_MESH)

  - Use a verb-noun method to name routines that perform some
    operation-on-a-given-object. Most names are constructed by concatenating
    several words, use mixed-case formatting or underscore to ease reading. \code
calculateKineticEnergy ( . . . )
calculate_kinetic_energy ( . . . )
\endcode
or any other derivatives.

  - Avoid elusive names, open to subjective interpretation like \code
Analyse ( . . . ) / / subroutine or function or method
nnsmcomp1 / / variable
\endcode

  - Each class header should be fully commented.
  
  - A \\file comment block at the top of the file; DO NOT include things like
    Author and Date blocks; this stuff is available from subversion if we
    really need to know.

  - Each function in both the public and private interfaces should be
    commented, INCLUDING ANY ARGUMENTS AND RETURN VALUES. See the MOAB classes
    for examples of how to format these comments. 

  - Developers should avoid using #include in header files, as they propagate
    dependencies more widely than necessary. 

  - Local variables and function argument have names with small letters, i.e.
    temperature_val, nodal_position. 

  - If variable is a pointer, it should have name as follows \code
class A {
  double *valPtr; ///< class member variable
};

double *val_ptr; ///< local variable
\endcode


  - Append/Prepend computation qualifiers like Av, Sum, Min, Max and Index to
    the end of a variable when appropriate.

  - If you commit your code remove are depreciated functions names. Use of OLD function name (or old functions
    arguments) generate compilation warring, f.e. \code
In file included from mofem_v0.2/finite_element_library/impl/DirichletBC.cpp:39:0:
mofem_v0.2/src/interfaces/FieldInterface.hpp: In member function 'PetscErrorCode MoFEM::FieldInterface::set_other_local_VecCreateGhost(const MoFEM::MoFEMProblem*, const string&, const string&, RowColData, Vec, InsertMode, ScatterMode, int)':
mofem_v0.2/src/interfaces/FieldInterface.hpp:1300:107: warning: 'PetscErrorCode MoFEM::FieldInterface::set_other_local_VecCreateGhost(const MoFEM::MoFEMProblem*, const string&, const string&, RowColData, Vec, InsertMode, ScatterMode, int)' is deprecated (declared at mofem_v0.2/src/interfaces/FieldInterface.hpp:1296) [-Wdeprecated-declarations]
     ierr = set_other_local_VecCreateGhost(problem_ptr,fiel_name,cpy_field_name,rc,V,mode,scatter_mode,verb); CHKERRQ(ierr);
\endcode

  - Constants and Macros
    - Don't use a pre-processor macro where a const variable or an inline or
      template function will suffice. There is absolutely benefit to the former
      over the later with modern compilers. Further, using macros bypasses
      typechecking that the compiler would otherwise do for you and if used in
      headers, introduce names into the global rather than MoFEM namespace.

    - Don't define constants that are already provided by standard libraries.
      For example, use M_PI as defined in math.h rather than defining your own
      constant.

- Each header file should have defined macro, following example \code
#ifndef __CLASS_NAME_HPP__
#define __CLASS_NAME_HPP__

class ClassName {

};

#endif //__CLASS_NAME_HPP__
\endcode

- Each function should have build in error checking, following example \code{.cpp}
PetscErrorCode fun() {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

ierr = fun(); CHKERRQ(ierr);
\endcode

- Memory allocation

  - USE VALGRIND. Valging is powerful tool to find execution errors, use it if your
    code behave differently on two different computers, or you get segmentation fault, ect. Valgrin can be used as follows \code
    valgrind --track-origins=yes ./program_name -my_file mesh.cub
    \endcode
    Use small mesh, i.e. small problem, when you run valgrind, it take some
    time. YOU NEED TO COMPILE CODE WITH -DCMAKE_BUILD_TYPE=Debug.

  - Keep array of objects in STL vectors, multi-indexes or any other Boost or
    STL data structures. AVOID ALLOCATING ARRAY OF OBJECTS ON HEAP USING REGULAR POINTERS.

  - Use smart pointers
    <http://www.boost.org/doc/libs/1_57_0/libs/smart_ptr/smart_ptr.htm>. If
    exsisting code uses regular pointer, take opportunity and change it to smart
    pointer.

  - Check program with line command argument -log_summary, verifing if number of created PTESc
    objects is equal to number of destroyed objects \code
Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Matrix     8              8      1526844     0
 Matrix Partitioning     1              1          660     0
           Index Set    40             40        42992     0
   IS L to G Mapping    11             11        32016     0
              Vector    68             68       361384     0
      Vector Scatter    16             16         9792     0
                SNES     1              1         1340     0
      SNESLineSearch     1              1          880     0
              DMSNES     1              1          680     0
       Krylov Solver     1              1        18960     0
     DMKSP interface     1              1          664     0
      Preconditioner     2              2         2104     0
    Distributed Mesh     2              2         9008     0
Star Forest Bipartite Graph     5              5         4136     0
     Discrete System     2              2         1632     0
              Viewer     1              0            0     0
\endcode



\section Making Repository Commits

As a general rule, developers should update frequently, and commit changes
often. However, the repository should always remain in a state where the code
can be compiled. Most of the time, the code should also successfully execute
"ctest" run from the top-level directory. If you commit code that violates this
principal, it should be your first priority to return the repository code to a
compilable state, and your second priority to make sure "ctest" runs without
errors.

Finished proration of work should be committed by pull-request to CDashTesting
branch. Commits to the CDashTesting branch should also come with a non-trivial,
useful, non-verbose log message. It is required that before pull request user
merge current CDashTesting branch and verify if all ctest run without fails. If
pull request is accepted it is user responsibility to verify results on
CDash server <http://cdash.eng.gla.ac.uk/cdash/>. The first priority will be to
eliminate compilation errors, completion warnings, failed tests and memory
leaks.

Some guidance about branches:
- Do not commits to other people branch. You can commit only to branches created by yourself.
- If you like to commit to other (not own created) branch, do pull request. 
- Before marking pull request, pull from branch to which you like to commit.
- Pull regularly form CDasgTesting branch.
- If you working on two different tasks make two different branches. This simplifies code revision. 

*/
