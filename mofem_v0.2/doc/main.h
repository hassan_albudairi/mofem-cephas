/*! \mainpage Index Page
 
\section Introduction

MoFEM (Mesh Oriented Finite Element Method) is a new finite element analysis
code tailored for the solution of multi-physics problems with arbitrary levels
of approximation, different levels of mesh refinement and optimised for
high-performance computing. It is design to be able manage complexities related
to heterogeneous order of approximations for L2,H1,H-div and H-curl spaces.

MoFEM is the blend of the Boost MultiIndex containers, MOAB (Mesh Oriented
Database) and PETSc (Portable, Extensible Toolkit for Scientific Computation).
MoFEM is developed in C++ and it is open-source software under the GNU Lesser
General Public License (see Legal Stuff). The current version of MoFEM has full
support for CUBIT/TRELIS for pre-processing and ParaView for
post-processing. MoFEM will supports other pre-processors, f.e. gMsh and is
currently in the process of full integration with MeshKit.

\section ThridParty Third party libraries

List of third party libraries and packages used by MoFEM:


- MOAB <http://www.mcs.anl.gov/~fathom/moab-docs/html/userguide.html>.
- PETSc <http://www.mcs.anl.gov/petsc/documentation/index.html>
- BOOST <http://www.boost.org>
	- uBlas <http://www.boost.org/doc/libs/release/libs/numeric/>

\section related_pages Related pages

- \ref coding_practice

- \ref faqs

- CDashTesting <http://cdash.eng.gla.ac.uk/cdash/>

\section License

MoFEM is free software: you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.


MoFEM is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.


You should have received a copy of the GNU Lesser General Public License
along with MoFEM. If not, see <http://www.gnu.org/licenses/>

*/


