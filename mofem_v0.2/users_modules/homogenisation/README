Check before run
================

* Check where is your mesh file
* Check what verion of openmpi (or other MPI library) are you using

Example run simple cube RVE for validaiton
============================================================

mpirun -np 1 ./Cube_Disp -my_file ../users_modules/homogenisation/meshes/Cube.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./Cube_Trac -my_file ../users_modules/homogenisation/meshes/Cube.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./Cube_Periodic -my_file ../users_modules/homogenisation/meshes/Cube.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0

Example run simple cube with inclusion or reinforcemnt RVE
============================================================
mpirun -np 1 ./Cube_With_Inclusion_Disp -my_file ../users_modules/homogenisation/meshes/Cube_With_Inclusion.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./Cube_With_Inclusion_Traction -my_file ../users_modules/homogenisation/meshes/Cube_With_Inclusion.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./Cube_With_Inclusion_Periodic -my_file ../users_modules/homogenisation/meshes/Cube_With_Inclusion.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0


Example run textile RVE with trans-isotropic fibres (with no interface elements)
  - first use the potential flow to calculate the fibres directions
============================================================
mpirun -np 1 ./TransIso_Potential_Flow_NoInt -my_file /mnt/home/Documents/Post-Doc\ Data/Analysis/Meshes/Trans_RVE/RVE.cub  -ksp_type fgmres -ksp_gmres_restart 1000 -pc_type asm -sub_pc_type lu -ksp_monitor -my_order 1

mpirun -np 1 ./TransIso_Disp_NoInt -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./TransIso_Periodic_NoInt -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./TransIso_Trac_NoInt -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0

Example run textile RVE with trans-isotropic fibres (with interface elements)
  - first use the potential flow to calculate the fibres directions
============================================================
mpirun -np 1 ./TransIso_Potential_Flow -my_file /mnt/home/Documents/Post-Doc\ Data/Analysis/Meshes/Trans_RVE/RVE.cub fgmres -ksp_gmres_restart 1000 -pc_type asm -sub_pc_type lu -ksp_monitor -my_order 1
mpirun -np 1 ./TransIso_Disp -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./TransIso_Periodic -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0
mpirun -np 1 ./TransIso_Trac -my_file solution_RVE.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0,0.0,0.0,0.0


Example computational homogenisation for moisture transport
============================================================
mpirun -np 1 ./MoistureTrans -my_file ../users_modules/homogenisation/meshes/Simple_moisture.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1

make && mpirun -np 1 ./MoistureTrans_Disp -my_file ../users_modules/homogenisation/meshes/Cube_moisture.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0

mpirun -np 1 ./MoistureTrans_Trac -my_file ../users_modules/homogenisation/meshes/Cube_moisture.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0

make && mpirun -np 1 ./MoistureTrans_Periodic -my_file ../users_modules/homogenisation/meshes/Cube_moisture.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1 -myapplied_strain 1.0,0.0,0.0


FE2 Elasticity
=======================================================
make FE2_Elasticity && mpirun -np 1 ./FE2_Elasticity -my_file_macro ../users_modules/homogenisation/meshes/LShape.cub -my_file_RVE ../users_modules/homogenisation/meshes/Cube.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package superlu_dist -ksp_monitor -my_order 1



copy out put files to symlink on Desktop
-------------------------------------------------------------
cp out.vtk /mnt/home/Desktop/
cp *.vtk  /mnt/home/Desktop/


