#!/bin/sh

set -e

ARGS=$@

while test $# -gt 0; do
        case "$1" in
                -h|--help)
                        echo "convergence_study"
                        echo " "
                        echo "convergence_study [arguments]"
                        echo " "
                        echo "options:"
                        echo "-h, --help"
                        echo "-f, --file_name=FILE"
			echo "-l, --load_factor=LOAD_FACTOR"
			echo "-g, --gc=GRIFFITH_ENERGY"
			echo "-o, --orders=1,2,3,4,5 (max is 5)\"" 
			echo "-r, --ref=0,1,2,3,4 ... (memory and time is your limit)\""
			echo "-d, --dir=DIR_NAME"
      			echo "-t, --rol=TOLERANCE"
                        exit 0
                        ;;
		--showme) 
			export SHOWME=1
			shift
			;;
                -f)
                        shift
                        if test $# -gt 0; then
                                export FILE=$1
                        else
                                echo "no file name specified"
                                exit 1
                        fi
                        shift
                        ;;
                --file_name*)
                        export FILE=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
                -d)
                        shift
                        if test $# -gt 0; then
                                export DIR_NAME=$1
                        else
                                echo "no file name specified"
                                exit 1
                        fi
                        shift
                        ;;
                --dir*)
                        export DIR_NAME=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;

                -l)
                        shift
                        if test $# -gt 0; then
                                export LOAD_FACTOR=$1
                        else
                                echo "no load factor specified"
                                exit 1
                        fi
                        shift
                        ;;
                --load_factor*)
                        export LOAD_FACTOR=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-g)
                        shift
                        if test $# -gt 0; then
                                export GC=$1
                        else
                                echo "no griffith energy specified"
                                exit 1
                        fi
                        shift
                        ;;
                --gc*)
                        export GC=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-o)
                        shift
                        if test $# -gt 0; then
                                export ORDERS=$1
                        else
                                echo "no griffith energy specified"
                                exit 1
                        fi
                        shift
                        ;;
                --orders*)
                        export ORDERS=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-r)
                        shift
                        if test $# -gt 0; then
                                export REF=$1
                        else
                                echo "no griffith energy specified"
                                exit 1
                        fi
                        shift
                        ;;
                --ref*)
                        export REF=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;

		-p)
                        shift
                        if test $# -gt 0; then
                                export NB_PROC=$1
                        else
                                echo "no griffith energy specified"
                                exit 1
                        fi
                        shift
                        ;;
                --proc*)
                        export NB_PROC=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		--pc_factor_mat_solver_package*) 
                        export PCFACTOR=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-t)
                        shift
                        if test $# -gt 0; then
                                export TOL=$1
                        else
                                echo "no tolerance specified"
                                exit 1
                        fi
                        shift
                        ;;
                --tol*)
                        export TOL=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
                *)
                        break
                        ;;
        esac
done

if [ -z ${SHOWME+x} ]; then
  echo "Line command ..." | tee log
  echo $0 $ARGS | tee -a log
  echo | tee -a log
  echo "Satrt time: "`date` | tee -a log
fi

if [ -z ${FILE+x} ]; then 
  echo "Mesh file is unset, convergence_study --help"
fi

if [ -z ${LOAD_FACTOR+x} ]; then 
  echo "Load factor is unset, convergence_study --help"
  exit 1
fi

if [ -z ${ORDERS+x} ]; then 
  echo "Approximation order is unset, convergence_study --help"
  exit 1
fi

if [ -z ${REF+x} ]; then 
  echo "Refinement levels is unset, convergence_study --help"
  exit 1
fi

if [ -z ${NB_PROC+x} ]; then 
  NB_PROC=4
fi

if [ -z ${PCFACTOR+x} ]; then 
  PCFACTOR=superlu_dist
fi

OPTIONS_MATERIAL_FORCE="-pc_factor_mat_solver_package $PCFACTOR"

if [ ! -z ${GC+x} ]; then 
  OPTIONS_MATERIAL_FORCE="$OPTIONS_MATERIAL_FORCE -my_gc $GC"
fi

if [ -z ${TOL+x} ]; then 
  TOL=1e-9
fi

BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
MPIRUN="@MoFEM_MPI_RUN@ -np $NB_PROC" 
MPIRUN_ONE_PROC="@MoFEM_MPI_RUN@ -np 1" 


if [ -z ${DIR_NAME+x} ]; then 
  DIR_SURFIX=`date "+DATE-%Y-%m-%d_TIME_%H_%M_%S"`
  DIR_PREFIX=`basename $FILE .cub`
  DIR_PREFIX=`basename $DIR_PREFIX .h5m`
  export DIR_NAME=material_forces_"$DIR_PREFIX"_"$DIR_SURFIX"
fi

echo "mkdir -p $DIR_NAME"
if [ -z ${SHOWME+x} ]; then
  if [ ! -d $DIR_NAME ]; then
    mkdir $DIR_NAME
  fi
fi

for order in `echo $ORDERS | sed -e 's/,/ /g'`
do
  for ref_level in `echo $REF | sed -e 's/,/ /g'`
  do

    if [ -z ${SHOWME+x} ]; then
      echo | tee -a $DIR_NAME/log_griffith_forces
      echo average griffith force order $order ref level $ref_level | tee -a $DIR_NAME/log_griffith_forces
      echo | tee -a $DIR_NAME/log_griffith_forces
    fi

    COMMAND1="$MPIRUN $BIN_PATH/spatial_problem \
      -my_file $FILE -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package $PCFACTOR \
      -ksp_atol 1e-14 -ksp_rtol 1e-12 -ksp_monitor \
      -snes_monitor -snes_type newtonls -snes_linesearch_type l2 -snes_max_it 10 -snes_atol $TOL -snes_rtol 1e-9 -snes_stol 0 \
      -my_order $order -my_ref $ref_level -my_load $LOAD_FACTOR -my_max_post_proc_ref_level 0 -log_summary"

    echo "$COMMAND1 2>&1 | tee -a $DIR_NAME/log_solution"
    if [ -z ${SHOWME+x} ]; then
      $COMMAND1 2>&1 | tee -a $DIR_NAME/log_solution
    fi 

    COMMAND2="$MPIRUN_ONE_PROC $BIN_PATH/material_force \
      -my_file out_spatial.h5m \
	-ksp_type gmres -pc_type lu -ksp_atol 1e-14 -ksp_rtol 1e-12 \
	-ksp_monitor -log_summary \
	-my_gc $GC -pc_factor_mat_solver_package $PCFACTOR"

    echo "$COMMAND2 2>&1 | tee -a $DIR_NAME/log_griffith_forces" 
    if [ -z ${SHOWME+x} ]; then
      $COMMAND2 2>&1 | tee -a $DIR_NAME/log_griffith_forces
    fi 

    if [ -z ${SHOWME+x} ]; then
      mv out.vtk $DIR_NAME/out_"$order"_"$ref_level".vtk 
      mv out_surface.vtk $DIR_NAME/out_surface_"$order"_"$ref_level".vtk 
      mv out_crack_surface.vtk $DIR_NAME/out_crack_surface_"$order"_"$ref_level".vtk 
      mv out_stresses.vtk $DIR_NAME/out_stresses_"$order"_"$ref_level".vtk 
      mv out_spatial.h5m $DIR_NAME/out_spatial_"$order"_"$ref_level".h5m
    fi

    if [ ! -z ${SHOWME+x} ]; then
      echo mv out.vtk $DIR_NAME/out_"$order"_"$ref_level".vtk 
      echo mv out_surface.vtk $DIR_NAME/out_surface_"$order"_"$ref_level".vtk 
      echo mv out_crack_surface.vtk $DIR_NAME/out_crack_surface_"$order"_"$ref_level".vtk 
      echo mv out_stresses.vtk $DIR_NAME/out_stresses_"$order"_"$ref_level".vtk 
      echo mv out_spatial.h5m $DIR_NAME/out_spatial_"$order"_"$ref_level".h5m
    fi


  done

done

