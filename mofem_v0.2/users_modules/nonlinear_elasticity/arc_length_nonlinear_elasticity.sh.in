#!/bin/sh

ARGS=$@

while test $# -gt 0; do
        case "$1" in
                -h|--help)
                        echo "arc_length_nonlinear_elasticity.sh"
                        echo " "
                        echo "arc_length_nonlinear_elasticity.sh [arguments]"
                        echo " "
                        echo "options:"
                        echo "-h, --help"
                        echo "-f, --file_name=FILE"
			echo "-n, --nb_steps=NUMBER_OF_STEPS"
			echo "-s, --step_size=STEP_SIZE"
			echo "-w, --imperfaction_factor=IF"
			echo "-d, --dir=DIR_NAME"
			echo "-p, --proc=NB_OF_PROCESSORS"
      			echo "-t, --rol=TOLERANCE"
			echo "-o, --order=1,2,3,4,5 (max is 5)\"" 
                        exit 0
                        ;;
                -f)
                        shift
                        if test $# -gt 0; then
                                export FILE=$1
                        else
                                echo "no file name specified"
                                exit 1
                        fi
                        shift
                        ;;
                --file_name*)
                        export FILE=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
                -d)
                        shift
                        if test $# -gt 0; then
                                export DIR_NAME=$1
                        else
                                echo "no file name specified"
                                exit 1
                        fi
                        shift
                        ;;
                --dir*)
                        export DIR_NAME=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;

		-n)
                        shift
                        if test $# -gt 0; then
                                export NBSTEPS=$1
                        else
                                echo "no number of steps specified"
                                exit 1
                        fi
                        shift
                        ;;
                --nb_steps*)
                        export NBSTEPS=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-s)
                        shift
                        if test $# -gt 0; then
                                export STEP_SIZE=$1
                        else
                                echo "no number of steps specified"
                                exit 1
                        fi
                        shift
                        ;;
                --step_size*)
                        export STEP_SIZE=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-w)
                        shift
                        if test $# -gt 0; then
                                export IF=$1
                        else
                                echo "imperfaection factor"
                                exit 1
                        fi
                        shift
                        ;;
                --imperfection_factor*)
                        export IF=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;

		-p)
                        shift
                        if test $# -gt 0; then
                                export NB_PROC=$1
                        else
                                echo "no processors number specified"
                                exit 1
                        fi
                        shift
                        ;;
                --proc*)
                        export NB_PROC=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-t)
                        shift
                        if test $# -gt 0; then
                                export TOL=$1
                        else
                                echo "no tolerance specified"
                                exit 1
                        fi
                        shift
                        ;;
                --tol*)
                        export TOL=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-i)
                        shift
                        if test $# -gt 0; then
                                export ITSD=$1
                        else
                                echo "no desired number of iterations specified"
                                exit 1
                        fi
                        shift
                        ;;
                --its_d*)
                        export ITSD=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		--snes_linesearch_type*) 
                        export LINESEARCHTYPE=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		--pc_factor_mat_solver_package*) 
                        export PCFACTOR=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;
		-o)
                        shift
                        if test $# -gt 0; then
                                export ORDER=$1
                        else
                                echo "no griffith energy specified"
                                exit 1
                        fi
                        shift
                        ;;
                --order*)
                        export ORDER=`echo $1 | sed -e 's/^[^=]*=//g'`
                        shift
                        ;;

                *)
                        break
                        ;;
        esac
done

if [ -z \$\{FILE+x\} ]; then 
  echo "Mesh file is unset, arc_length --help"
fi

if [ -z ${STEP_SIZE+x} ]; then 
  echo "Setep size is unset, arc_length --help"
  exit 1
fi

if [ -z $NBSTEPS+x} ]; then 
  echo "Number of steps is unset, arc_length --help"
  exit 1
fi

if [ -z ${DIR_NAME+x} ]; then
  DIR_SURFIX=`date "+DATE-%Y-%m-%d_TIME_%H_%M_%S"`
  DIR_PREFIX=`basename $FILE .h5m`
  export DIR_NAME=arc_length_"$DIR_PREFIX"_"$DIR_SURFIX"
fi

if [ ! -d $DIR_NAME ]; then
  mkdir $DIR_NAME
fi

if [ -z ${NB_PROC+x} ]; then 
  NB_PROC=1
fi

if [ -z ${TOL+x} ]; then 
  TOL=1e-9
fi

if [ -z ${ITSD+x} ]; then 
  ITSD=6
fi

if [ -z ${LINESEARCHTYPE+x} ]; then 
  LINESEARCHTYPE="basic"
fi

if [ -z ${PCFACTOR+x} ]; then 
  PCFACTOR=superlu_dist
fi

if [ -z ${DESIRED_NUMBER_OF_ITS+x} ]; then 
  DESIRED_NUMBER_OF_ITS=4
fi

if [ -z ${ORDER+x} ]; then 
  ORDER=2
fi

if [ -z ${IF+x} ]; then 
  IF=1
fi


BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
MPIRUN="@MPI_RUN@ -np $NB_PROC"

cp $FILE $DIR_NAME/
cd $DIR_NAME

echo "Line command ..." | tee log
echo $0 $ARGS | tee -a log
echo | tee -a log
echo "Satrt time: "`date` | tee -a log

COMMAND="$MPIRUN $BIN_PATH/arc_length_nonlinear_elasticity \
  -my_file `basename $FILE` \
  -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package $PCFACTOR \
  -snes_type newtonls -snes_linesearch_type $LINESEARCHTYPE -snes_linesearch_monitor \
  -ksp_atol 1e-14 -ksp_rtol 1e-12 -ksp_max_it 500\
  -snes_max_it 25 -snes_atol 1e-9 -snes_rtol 1e-9 -snes_stol 0 \
  -snes_monitor -snes_converged_reason -ksp_monitor \
  -my_tol $TOL -my_its_d $ITSD \
  -ms $NBSTEPS -my_sr $STEP_SIZE -my_if $IF \
  -my_order $ORDER  \
  -log_summary"

echo | tee -a log
echo "Start calculations ..." | tee -a log
echo $COMMAND | tee -a log
echo | tee -a log

exec $COMMAND 2>&1 | tee -a log

echo | tee -a log
echo "End time: "`date` | tee -a log


if [ $? -ne 0 ]; then
  exit $?
fi


cd ..
