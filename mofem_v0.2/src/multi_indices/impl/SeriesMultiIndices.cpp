/** \file SeriesMultiIndices.cpp
 * \brief Data strutures for time steries and load series storage,
 *  
 * Copyright (C) 2013, Lukasz Kaczmarczyk (likask AT wp.pl) <br>
 *
 * The MoFEM package is copyrighted by Lukasz Kaczmarczyk. 
 * It can be freely used for educational and research purposes 
 * by other institutions. If you use this softwre pleas cite my work. 
 *
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>
*/

#include <petscsys.h>
#include <cblas.h>

#include <definitions.h>
#include <h1_hdiv_hcurl_l2.h>

#include <Common.hpp>
#include <CoreDataStructures.hpp>

namespace MoFEM {

MoFEMSeries::MoFEMSeries(Interface &moab,const EntityHandle _meshset): 
  meshset(_meshset),tag_name_data(NULL),tag_name_size(0),
  record_begin(false),record_end(false) {
  ErrorCode rval;

  Tag th_SeriesName;
  rval = moab.tag_get_handle("_SeriesName",th_SeriesName); CHKERR(rval);
  rval = moab.tag_get_by_ptr(th_SeriesName,&meshset,1,(const void **)&tag_name_data,&tag_name_size); CHKERR_THROW(rval);
 
  const int def_val_len = 0;

  //time
  string Tag_SeriesTime = "_SeriesTime_"+get_name();
  double def_time = 0;
  rval = moab.tag_get_handle(Tag_SeriesTime.c_str(),1,MB_TYPE_DOUBLE,
    th_SeriesTime,MB_TAG_CREAT|MB_TAG_SPARSE,&def_time); CHKERR_THROW(rval);

  //handles
  string Tag_DataHandles_SeriesName = "_SeriesDataHandles_"+get_name();
  rval = moab.tag_get_handle(Tag_DataHandles_SeriesName.c_str(),def_val_len,MB_TYPE_HANDLE,
    th_SeriesDataHandles,MB_TAG_CREAT|MB_TAG_SPARSE|MB_TAG_VARLEN,NULL); CHKERR_THROW(rval);
 
  //uids
  string Tag_DataUIDs_SeriesName = "_SeriesDataUIDs_"+get_name();
  rval = moab.tag_get_handle(Tag_DataUIDs_SeriesName.c_str(),def_val_len,MB_TYPE_OPAQUE,
    th_SeriesDataUIDs,MB_TAG_CREAT|MB_TAG_SPARSE|MB_TAG_BYTES|MB_TAG_VARLEN,NULL); CHKERR_THROW(rval);

  //data
  string Tag_Data_SeriesName = "_SeriesData_"+get_name();
  rval = moab.tag_get_handle(Tag_Data_SeriesName.c_str(),def_val_len,MB_TYPE_OPAQUE,
    th_SeriesData,MB_TAG_CREAT|MB_TAG_SPARSE|MB_TAG_BYTES|MB_TAG_VARLEN,NULL); CHKERR_THROW(rval);

}

PetscErrorCode MoFEMSeries::get_nb_steps(Interface &moab,int &nb_steps) const {
  PetscFunctionBegin;
  ErrorCode rval;
  rval = moab.num_contained_meshsets(meshset,&nb_steps); CHKERR_PETSC(rval);
  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeries::push_dofs(const EntityHandle ent,const ShortId uid,const FieldData val) {
  PetscFunctionBegin;
  if(!record_begin) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"you neet to set recording");
  }
  handles.push_back(ent);
  uids.push_back(uid);
  data.push_back(val);
  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeries::begin() { 
  PetscFunctionBegin;
  if(record_begin) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"recording already  begin");
  }
  record_begin = true;
  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeries::end(double t) { 
  PetscFunctionBegin;
  if(!record_begin) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"recording not begin it can not be ended");
  }
  record_begin = false;
  record_end = true;
  ia.push_back(uids.size());
  time.push_back(t);
  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeries::read(Interface &moab) {
  PetscFunctionBegin;
  ErrorCode rval;

  if(record_begin) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"all series data will be lost");
  }
  if(record_end) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"all series data will be lost");
  }

  vector<EntityHandle> contained;
  rval = moab.get_contained_meshsets(meshset,contained); CHKERR_PETSC(rval);
  ia.resize(0);
  time.resize(0);
  handles.resize(0);
  uids.resize(0);
  data.resize(0);
  ia.push_back(0);

  for(unsigned int mm = 0;mm<contained.size();mm++) {
    //time 
    {
      double t;
      rval = moab.tag_set_data(th_SeriesTime,&meshset,1,&t);  CHKERR(rval);
      time.push_back(t);
    }
    //handles
    {
      const EntityHandle* tag_data;
      int tag_size;
      rval = moab.tag_get_by_ptr(th_SeriesDataHandles,&meshset,1,(const void **)&tag_data,&tag_size); CHKERR_PETSC(rval);
      handles.insert(handles.end(),tag_data,&tag_data[tag_size]);
    }
    //uids
    {
      const ShortId* tag_data;
      int tag_size;
      rval = moab.tag_get_by_ptr(th_SeriesDataUIDs,&meshset,1,(const void **)&tag_data,&tag_size); CHKERR_PETSC(rval);
      int nb = tag_size/sizeof(ShortId);
      uids.insert(uids.end(),tag_data,&tag_data[nb]);
    }
    if(handles.size() != uids.size()) {
      SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
    }
    //data
    {
      const FieldData* tag_data;
      int tag_size;
      rval = moab.tag_get_by_ptr(th_SeriesData,&meshset,1,(const void **)&tag_data,&tag_size); CHKERR_PETSC(rval);
      int nb = tag_size/sizeof(FieldData);
      data.insert(data.end(),tag_data,&tag_data[nb]);
    }
    if(data.size() != uids.size()) {
      SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
    }
    ia.push_back(data.size());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeries::save(Interface &moab) const {
  PetscFunctionBegin;

  if(record_begin) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"switch off recording");
  }
  if(!record_end) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"finish recording");
  }

  ErrorCode rval;
  vector<EntityHandle> contained;
  rval = moab.get_contained_meshsets(meshset,contained); CHKERR_PETSC(rval);
  unsigned int nb_contained = contained.size();
  if(nb_contained < ia.size()-1) {
    contained.resize(ia.size());
  }
  for(unsigned int mm = ia.size()-1;mm<nb_contained;mm++) {
    rval = moab.remove_entities(meshset,&contained[mm],1); CHKERR_PETSC(rval);
    rval = moab.delete_entities(&contained[mm],1); CHKERR_PETSC(rval);
  }
  for(unsigned int mm = nb_contained;mm<ia.size()-1;mm++) {
    EntityHandle new_meshset;
    rval = moab.create_meshset(MESHSET_SET,new_meshset); CHKERR_PETSC(rval);
    rval = moab.add_entities(meshset,&new_meshset,1); CHKERR_PETSC(rval);
  }
  contained.resize(0);
  rval = moab.get_contained_meshsets(meshset,contained); CHKERR_PETSC(rval);
  if(contained.size() != ia.size()-1) {
    SETERRQ2(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency nb_contained != ia.size()-1 %d!=%d",contained.size(),ia.size()-1);
  }

  //time
  for(unsigned int ii = 1;ii<ia.size();ii++) {
    rval = moab.tag_set_data(th_SeriesTime,&contained[ii-1],1,&time[ii-1]);  CHKERR(rval);
  }

  //handles
  for(unsigned int ii = 1;ii<ia.size();ii++) {
    void const* tag_data[] = { &handles[ia[ii-1]] };
    int tag_sizes[] = { (ia[ii]-ia[ii-1]) };
    rval = moab.tag_set_by_ptr(th_SeriesDataHandles,&contained[ii-1],1,tag_data,tag_sizes); CHKERR_PETSC(rval);
  }
  //uids
  for(unsigned int ii = 1;ii<ia.size();ii++) {
    void const* tag_data[] = { &uids[ia[ii-1]] };
    int tag_sizes[] = { (ia[ii]-ia[ii-1])*sizeof(ShortId) };
    rval = moab.tag_set_by_ptr(th_SeriesDataUIDs,&contained[ii-1],1,tag_data,tag_sizes); CHKERR_PETSC(rval);
  }
  
  //data
  for(unsigned int ii = 1;ii<ia.size();ii++) {
    void const* tag_data[] = { &data[ia[ii-1]] };
    int tag_sizes[] = { (ia[ii]-ia[ii-1])*sizeof(FieldData) };
    rval = moab.tag_set_by_ptr(th_SeriesData,&contained[ii-1],1,tag_data,tag_sizes); CHKERR_PETSC(rval);
  }

  PetscFunctionReturn(0);
}

MoFEMSeriesStep::MoFEMSeriesStep(Interface &moab,const MoFEMSeries *_MoFEMSeries_ptr,const int _step_number): 
  interface_MoFEMSeries<MoFEMSeries>(_MoFEMSeries_ptr),step_number(_step_number) {
  PetscErrorCode ierr;
  ierr = get_time_init(moab); CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

PetscErrorCode MoFEMSeriesStep::get(Interface &moab,DofMoFEMEntity_multiIndex &dofsMoabField) const {
  PetscFunctionBegin;
  ErrorCode rval;

  vector<EntityHandle> contained;
  rval = moab.get_contained_meshsets(ptr->meshset,contained); CHKERR_PETSC(rval);
  if(contained.size()<=(unsigned int)step_number) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
  }

  EntityHandle *handles_ptr;
  int handles_size;
  rval = moab.tag_get_by_ptr(ptr->th_SeriesDataHandles,&contained[step_number],1,(const void **)&handles_ptr,&handles_size); CHKERR_PETSC(rval);

  ShortId *uids_ptr;
  int uids_size;
  rval = moab.tag_get_by_ptr(ptr->th_SeriesDataUIDs,&contained[step_number],1,(const void **)&uids_ptr,&uids_size); CHKERR_PETSC(rval);
  uids_size /= sizeof(ShortId);

  if(handles_size != uids_size) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
  }

  FieldData *data_ptr; 
  int data_size;
  rval = moab.tag_get_by_ptr(ptr->th_SeriesData,&contained[step_number],1,(const void **)&data_ptr,&data_size); CHKERR_PETSC(rval);
  data_size /= sizeof(FieldData);

  if(data_size != uids_size) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
  }

  for(int ii = 0;ii<uids_size;ii++) {
    EntityHandle ent = handles_ptr[ii];
    ShortId uid = uids_ptr[ii];
    FieldData val = data_ptr[ii];
    DofMoFEMEntity_multiIndex::index<Composite_Entity_and_ShortId_mi_tag>::type::iterator dit;
    dit = dofsMoabField.get<Composite_Entity_and_ShortId_mi_tag>().find(boost::make_tuple(ent,uid));
    if(dit!=dofsMoabField.get<Composite_Entity_and_ShortId_mi_tag>().end()) {
      //cerr << *dit << endl;
      dit->get_FieldData() = val;
    } else {
      SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_FOUND,
	"data inconsistency, getting data series, dof on ENTITY and ShortId can't be found");
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode MoFEMSeriesStep::get_time_init(Interface &moab) {
  PetscFunctionBegin;
  ErrorCode rval;
  vector<EntityHandle> contained;
  rval = moab.get_contained_meshsets(ptr->meshset,contained); CHKERR_PETSC(rval);
  if(contained.size()<=(unsigned int)step_number) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"data inconsistency");
  }
  double *time_ptr;
  int size;
  rval = moab.tag_get_by_ptr(ptr->th_SeriesTime,&contained[step_number],1,(const void **)&time_ptr,&size); CHKERR_PETSC(rval);
  time = *time_ptr;
  PetscFunctionReturn(0);
}

ostream& operator<<(ostream& os,const MoFEMSeries& e) {
  os << "name " << e.get_name() << " meshset " << e.get_meshset();
  return os;
}

ostream& operator<<(ostream& os,const MoFEMSeriesStep& e) {
  os << *(e.get_MoFEMSeries_ptr()) << " step number " << e.step_number << " time " << e.get_time();
  return os;
}


}
